﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private string[] titles;
    [SerializeField]
    private Sprite[] uiSprites;
    private int index;
    private int minLength;
    
    public Image MainImage;
    public Text Title;

    // Start is called before the first frame update
    void Start()
    {
        //Check arrays lenght:
        minLength = (titles.Length > uiSprites.Length)? uiSprites.Length: titles.Length;
        index = 0;

        MainImage.sprite = uiSprites[index];
        Title.text = titles[index];
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ButtonPress(bool next)
    {
        int i = (next) ? 1 : -1;
        index = index + 1;
        if (index >= minLength)
        {
            index = 0;
        }
        else if(index < 0)
        {
           index = minLength - 1;
        }


        Debug.Log($"{next} - {index} - {i} - {index + i}");
        changeImage();
        changeTitles();
    }

    private void changeTitles()
    {
        MainImage.sprite = uiSprites[index];
    }

    private void changeImage()
    {
        Title.text = titles[index];
    }

}

